'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// Подключение самописных файлов
var ProgramFilter = function () {
    function ProgramFilter($filter) {
        _classCallCheck(this, ProgramFilter);

        this.$filter = $filter;

        this.bindEvents();
    }

    /**
     * События
     */


    _createClass(ProgramFilter, [{
        key: 'bindEvents',
        value: function bindEvents() {
            this.checkInCart();
            this.openTooltip();
        }

        /**
         * Изменения состояния чекбокса в корзину
         */

    }, {
        key: 'checkInCart',
        value: function checkInCart() {
            $('body').on('click', '.js-in-cart', function (e) {
                var $this = $(e.currentTarget),
                    $input = $this.siblings('input'),
                    $txt = $this.find('.js-in-cart-txt'),
                    isClicked = !$input.prop('checked');

                isClicked ? $txt.text('входит') : $txt.text('в корзину');
            });
        }

        /**
         * Открыть тултип со списком программ
         */

    }, {
        key: 'openTooltip',
        value: function openTooltip() {
            $('body').on('click', '.js-open-tooltip', function (e) {
                var $this = $(e.currentTarget),
                    $tooltip = $this.siblings('.js-programs-tooltip'),
                    $parentLi = $this.closest('li'),
                    $allLink = $parentLi.siblings('li').find('a');

                e.preventDefault();
                $this.addClass('is-active');
                $this.hasClass('is-active') ? $tooltip.show() : $tooltip.hide();

                $(document).mouseup(function (e) {
                    if (!$tooltip.is(e.target) && $tooltip.has(e.target).length === 0) {
                        if ($allLink.hasClass('is-active')) {
                            $this.removeClass('is-active');
                        }
                        $tooltip.hide();
                    }
                });
            });
        }
    }]);

    return ProgramFilter;
}();

var Mask = function () {
    function Mask() {
        _classCallCheck(this, Mask);
    }

    _createClass(Mask, [{
        key: 'initPhoneMask',


        /**
         * Маска для номера телефона
         */
        value: function initPhoneMask() {
            $(".js-phone-mask").each(function () {
                $(this).mask("+7 (999) 999-99-99", { autoclear: false });
            });
        }
    }]);

    return Mask;
}();

var Lightbox = function () {
    function Lightbox($lightbox) {
        _classCallCheck(this, Lightbox);

        this.$lightbox = $lightbox;
        this.$mainThumb = $lightbox.find('.js-lightbox-main');
        this.$thumbs = $lightbox.find('.js-lightbox-thumb');
        this.$mainImg = this.$mainThumb.find('img');
        this.$mainLink = this.$mainThumb.find('a');

        this.bindEvents();
    }

    /**
     * События
     */


    _createClass(Lightbox, [{
        key: 'bindEvents',
        value: function bindEvents() {
            var that = this,
                mainImgSrc = this.$mainImg[0].src;

            this.$thumbs.each(function () {
                var _this = this;

                var $thumb = $(this);

                $thumb.on('click', function (e) {
                    var $this = $(_this),
                        $img = $this.find('img'),
                        imgSrc = $img[0].src;

                    e.preventDefault();

                    that.$mainImg[0].src = imgSrc;
                    that.$mainLink.attr('href', imgSrc);

                    $img[0].src = mainImgSrc;
                    mainImgSrc = imgSrc;
                });
            });
        }
    }]);

    return Lightbox;
}();

var Dropdown = function () {
    function Dropdown($dropdown) {
        _classCallCheck(this, Dropdown);

        this.$dropdown = $dropdown;
        this.$dropdownBody = $dropdown.find('.js-dropdown-body');

        this.bindEvents();
    }

    /**
     * События
     */


    _createClass(Dropdown, [{
        key: 'bindEvents',
        value: function bindEvents() {
            var that = this;

            this.$dropdown.on('click', function (e) {
                that.$dropdown.toggleClass('is-active');
                that.$dropdownBody.toggleClass('is-active');
            });

            $(document).mouseup(function (e) {
                if (!that.$dropdown.is(e.target) && that.$dropdown.has(e.target).length === 0) {
                    that.$dropdown.removeClass('is-active');
                    that.$dropdownBody.removeClass('is-active');
                }
            });
        }
    }]);

    return Dropdown;
}();

var MapSvg = function () {
    function MapSvg($map) {
        _classCallCheck(this, MapSvg);

        this.$map = $map;
        this.$mapRegions = $map.find('.js-svg-fo');
        this.$popup = $map.find('.js-svg-popup');

        this.bindEvents();
    }

    /**
     * События
     */


    _createClass(MapSvg, [{
        key: 'bindEvents',
        value: function bindEvents() {
            var that = this;

            this.$mapRegions.each(function () {
                var _this2 = this;

                var $region = $(this);

                $region.on('click', function (e) {
                    var $this = $(_this2),
                        foId = $this.attr('id'),
                        dataUrl = $this.data('url');

                    that.$popup.removeClass('is-active');

                    $.ajax({
                        url: dataUrl,
                        data: {
                            fo: foId
                        },
                        success: function success(data) {
                            that.$popup.html(data);
                            that.updatePopup();
                        }
                    });

                    that.$popup.addClass('is-active');
                    $this.addClass('is-active');
                });
            });

            $('body').on('click', '.js-svg-close', function (e) {
                that.$popup.removeClass('is-active');
                that.$mapRegions.removeClass('is-active');
            });

            $(document).mouseup(function (e) {
                if (!that.$popup.is(e.target) && that.$popup.has(e.target).length === 0) {
                    that.$popup.removeClass('is-active');
                    that.$mapRegions.removeClass('is-active');
                }
            });
        }

        /**
         * Обновление поп-апа после ajax запроса
         */

    }, {
        key: 'updatePopup',
        value: function updatePopup() {
            this.$popup = this.$map.find('.js-svg-popup');
        }
    }]);

    return MapSvg;
}();

var Select = function () {
    function Select($select) {
        _classCallCheck(this, Select);

        this.$select = $select.find('.js-select-object');
        this.$selectTarget = $select.find('.js-select-target');
        this.$selectHead = this.$select.find('.js-select-head');
        this.$selectLinks = this.$select.find('.js-select-link');

        this.bindEvents();
    }

    /**
     * События
     */


    _createClass(Select, [{
        key: 'bindEvents',
        value: function bindEvents() {
            var _this3 = this;

            var that = this;

            this.$selectHead.on('click', function (e) {
                e.preventDefault();
                that.$select.toggleClass('is-active');
            });

            this.$selectLinks.each(function () {
                var $link = $(this),
                    $target = $($link.attr('href'));

                $link.on('click', function (e) {
                    e.preventDefault();

                    that.$selectTarget.removeClass('is-active').hide();
                    $target.addClass('is-active').fadeIn();

                    that.$selectHead.text($link.text());
                    that.$select.removeClass('is-active');
                    that.$selectLinks.removeClass('is-hidden');
                    $link.addClass('is-hidden');
                });
            });

            $(document).mouseup(function (e) {
                if (!_this3.$select.is(e.target) && _this3.$select.has(e.target).length === 0) {
                    _this3.$select.removeClass('is-active');
                }
            });
        }
    }]);

    return Select;
}();

var Shadow = function () {
    function Shadow($shadowBlock) {
        _classCallCheck(this, Shadow);

        this.$shadowBlock = $shadowBlock;

        this.checkIsShadow();
        this.bindEvents();
    }

    /**
     * События
     */


    _createClass(Shadow, [{
        key: 'bindEvents',
        value: function bindEvents() {
            var that = this;

            this.$shadowBlock.on('scroll', function (e) {
                that.$shadowBlock.removeClass('is-scroll-end');
                that.checkIsShadow();
            });
        }

        /**
         * Нужна ли тень
         */

    }, {
        key: 'checkIsShadow',
        value: function checkIsShadow(isFirstCall) {
            var that = this,
                scrollPosition = that.$shadowBlock.scrollTop() + that.$shadowBlock.height(),
                scrollHeight = that.$shadowBlock[0].scrollHeight;

            if (scrollPosition >= scrollHeight) {
                that.$shadowBlock.addClass('is-scroll-end');
            }
        }
    }]);

    return Shadow;
}();

var Tab = function () {
    function Tab($tab) {
        _classCallCheck(this, Tab);

        this.$tab = $tab;
        this.$tabLinks = this.$tab.find('.js-tab-link');
        this.$tabTargets = this.$tab.find('.js-tab-target');

        this.bindEvents();
    }

    /**
     * События
     */


    _createClass(Tab, [{
        key: 'bindEvents',
        value: function bindEvents() {
            var that = this;

            this.$tabLinks.each(function () {
                var $link = $(this),
                    $target = $($link.attr('href'));

                $link.on('click', function (e) {
                    e.preventDefault();
                    that.$tabLinks.removeClass('is-active');
                    $link.addClass('is-active');
                    that.$tabTargets.removeClass('is-active').hide();
                    $target.addClass('is-active').fadeIn();
                });
            });
        }
    }]);

    return Tab;
}();

var Map = function () {
    function Map($map) {
        _classCallCheck(this, Map);

        this.$map = $map;
        this.mapId = this.$map.attr('id');
        this.map = {};

        this.initMap();
    }

    /**
     * Инициализация карты
     */


    _createClass(Map, [{
        key: 'initMap',
        value: function initMap() {
            var _this4 = this;

            ymaps.ready(function () {
                _this4.map = new ymaps.Map(_this4.mapId, {
                    center: [55.76, 37.64],
                    zoom: 7
                });
            });
        }
    }]);

    return Map;
}();

var Validation = function () {
    function Validation() {
        _classCallCheck(this, Validation);

        this.bindEvents();
    }

    /**
     * События
     */


    _createClass(Validation, [{
        key: 'bindEvents',
        value: function bindEvents() {
            var that = this;

            $('body').on('click', '.js-btn-submit', function (e) {
                var isValid = true,
                    $thisBtn = $(e.currentTarget),
                    $form = $thisBtn.closest('form'),
                    $fields = $form.find('[data-valid]');

                that.clearFormError($form);

                $fields.each(function () {
                    var $this = $(this),
                        type = $(this).data('type');

                    if (type === 'text') {
                        if (!that.checkText($this)) {
                            that.setError($this);
                            isValid = false;
                        }
                    }

                    if (type === 'phone') {
                        if (!that.checkPhone($this)) {
                            that.setError($this);
                            isValid = false;
                        }
                    }

                    if (type === 'email') {
                        if (!that.checkEmail($this)) {
                            that.setError($this);
                            isValid = false;
                        }
                    }
                });

                if (!isValid) {
                    e.preventDefault();
                } else {
                    var url = $form.attr('action'),
                        data = $form.serializeArray(),
                        $popup = $form.closest('.b-popup');

                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: data,
                        success: function success(data) {
                            var mask = new Mask();
                            $popup.html(data);
                            mask.initPhoneMask();
                        },
                        error: function error(data) {
                            $popup.html(data);
                        }
                    });
                }
            });
        }

        /**
         * Проверка текстового поля
         */

    }, {
        key: 'checkText',
        value: function checkText($field) {
            return $field.val().length > 0;
        }

        /**
         * Проверка телефона
         */

    }, {
        key: 'checkPhone',
        value: function checkPhone($field) {
            var regex = /^((8|\+7)[\-]?)?(\(?\d{3}\)?[\-]?)?[\d\-]{7}$/;
            return regex.test($field.val());
        }

        /**
         * Проверка email
         */

    }, {
        key: 'checkEmail',
        value: function checkEmail($field) {
            var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/;
            return regex.test($field.val());
        }

        /**
         * Добавить полю ошибку
         */

    }, {
        key: 'setError',
        value: function setError($field) {
            $field.addClass('is-error');
        }

        /**
         * Очистить форму от ошибок
         */

    }, {
        key: 'clearFormError',
        value: function clearFormError($form) {
            $form.find('.is-error').removeClass('is-error');
        }
    }]);

    return Validation;
}();

var Tooltip = function () {
    function Tooltip($tooltipLink) {
        _classCallCheck(this, Tooltip);

        this.$tooltipLink = $tooltipLink;
        this.$tooltip = this.$tooltipLink.closest('.js-tooltip');
        this.$tooltipBody = this.$tooltip.find('.js-tooltip-body');
        this.$tooltipClose = this.$tooltip.find('.js-tooltip-close');

        this.bindEvents();
    }

    /**
     * События
     */


    _createClass(Tooltip, [{
        key: 'bindEvents',
        value: function bindEvents() {
            var _this5 = this;

            this.$tooltipLink.on('click', function (e) {
                if ($(window).width() > 1020) {
                    e.preventDefault();
                    _this5.$tooltipBody.attr('style', '');

                    var windowWidth = $(window).width(),
                        linkWidth = _this5.$tooltipLink.outerWidth(),
                        linkLeft = _this5.$tooltipLink.offset().left,
                        right = linkLeft + linkWidth,
                        tooltipWidth = _this5.$tooltipBody.outerWidth();

                    if (windowWidth - right < tooltipWidth) {
                        var newWidth = windowWidth - right;
                        _this5.$tooltipBody.css({
                            'min-width': newWidth + 'px',
                            'width': newWidth + 'px'
                        });
                    }
                    _this5.$tooltip.addClass('is-active');
                }
            });

            this.$tooltipClose.on('click', function (e) {
                _this5.$tooltip.removeClass('is-active');
            });

            $(document).mouseup(function (e) {
                if (!_this5.$tooltip.is(e.target) && _this5.$tooltip.has(e.target).length === 0) {
                    _this5.$tooltip.removeClass('is-active');
                }
            });
        }
    }]);

    return Tooltip;
}();

var Participant = function () {
    function Participant($partLink) {
        _classCallCheck(this, Participant);

        this.$partLink = $partLink;
        this.$partBody = $($partLink.attr('href'));
        this.$menu = $('.js-menu');
        this.$menuRows = this.$menu.find('.js-menu-row');

        this.bindEvents();
    }

    /**
     * События
     */


    _createClass(Participant, [{
        key: 'bindEvents',
        value: function bindEvents() {
            var _this6 = this;

            var isOpen = false;

            this.$partLink.on('click', function (e) {
                var windowWidth = $(window).width();

                e.preventDefault();
                isOpen ? isOpen = false : isOpen = true;

                isOpen ? _this6.$partBody.fadeIn() : _this6.$partBody.hide();
                _this6.$partLink.toggleClass('is-open');

                if (windowWidth >= 1280) {
                    _this6.$menuRows.last().toggleClass('is-hidden');
                } else {
                    _this6.$menu.toggleClass('is-open');
                    _this6.$menuRows.toggleClass('is-hidden');
                }
            });
        }
    }]);

    return Participant;
}();

var Popup = function () {
    function Popup($popup) {
        _classCallCheck(this, Popup);

        this.$popup = $popup;
        this.type = $popup.data('type');

        if (this.type === 'img') {
            this.initImgPopup();
        } else {
            this.initPopup();
        }
        this.bindEvents();
    }

    /**
     * События
     */


    _createClass(Popup, [{
        key: 'bindEvents',
        value: function bindEvents() {
            $('body').on('click', '.js-popup-close', this.hidePopup);
        }

        /**
         * Инициализация попапа
         */

    }, {
        key: 'initPopup',
        value: function initPopup() {
            var that = this;

            this.$popup.magnificPopup({
                type: 'inline',
                showCloseBtn: false,
                fixedContentPos: true,
                callbacks: {
                    open: function open() {
                        that.openPopup(this.content);
                    }
                }
            });
        }

        /**
         * Инициализация галлерии
         */

    }, {
        key: 'initImgPopup',
        value: function initImgPopup() {
            var that = this;

            this.$popup.magnificPopup({
                type: 'image',
                closeOnContentClick: true,
                showCloseBtn: true,
                fixedContentPos: true,
                image: {
                    verticalFit: true
                },
                callbacks: {
                    open: function open() {
                        that.openPopup(this.content);
                    }
                }
            });
        }

        /**
         * Событие открытия
         */

    }, {
        key: 'openPopup',
        value: function openPopup(popup) {
            var $popup = popup;

            if (this.type === 'buy') {
                $popup.parent().addClass('tickets-popup');
            }

            if (this.type === 'video') {
                $popup.parent().addClass('video-popup');
            }

            if (this.type === 'form') {
                $popup.parent().addClass('form-popup');
            }

            if (this.type === 'img') {
                $popup.parent().addClass('img-popup');
            }
        }

        /**
         * Закрытие попапа
         */

    }, {
        key: 'hidePopup',
        value: function hidePopup() {
            $.magnificPopup.close();
        }
    }]);

    return Popup;
}();

var Slider = function () {
    function Slider($slider) {
        _classCallCheck(this, Slider);

        this.$slider = $slider;
        this.sliderType = this.$slider.data('type');

        if (this.sliderType === 'expert') {
            this.initExpertSlider();
            this.bindExpertEvents();
        } else if (this.sliderType === 'forum') {
            this.bindForumEvents();
        } else if (this.sliderType === 'partners') {
            this.initPartnersSlider();
        } else {
            this.initSlider();
        }
    }

    /**
     * Инициализация стандартного слайдера
     */


    _createClass(Slider, [{
        key: 'initSlider',
        value: function initSlider() {
            this.$slider.slick({
                dots: true,
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                infinite: true,
                draggable: false,
                swipe: false
            });
        }

        /**
         * Инициализация слайдера экспертов
         */

    }, {
        key: 'initExpertSlider',
        value: function initExpertSlider() {
            var $sliderSync = $('.js-slider-sync');

            this.$slider.slick({
                dots: false,
                arrows: false,
                fade: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                asNavFor: '.js-experts',
                draggable: false,
                swipe: false
            });

            $sliderSync.slick({
                dots: false,
                arrows: false,
                slidesToShow: 5,
                responsive: [{
                    breakpoint: 1440,
                    settings: {
                        slidesToShow: 3
                    }
                }],
                slidesToScroll: 1,
                asNavFor: '.js-expert',
                centerMode: true,
                focusOnSelect: true,
                draggable: false,
                swipe: false
            });
        }

        /**
         * События слайдера экспертов
         */

    }, {
        key: 'bindExpertEvents',
        value: function bindExpertEvents() {
            var $sliderSync = $('.js-slider-sync'),
                $sliderNext = this.$slider.closest('.js-slider-experts').find('.js-slider-next'),
                $sliderPrev = this.$slider.closest('.js-slider-experts').find('.js-slider-prev');

            $sliderNext.on('click', function (e) {
                $sliderSync.slick('slickNext');
            });

            $sliderPrev.on('click', function (e) {
                $sliderSync.slick('slickPrev');
            });
        }

        /**
         * Инициализация слайдера форума
         */

    }, {
        key: 'initForumSlider',
        value: function initForumSlider() {
            if ($(window).width() < 960) {
                this.destroySlider();
                this.$slider.slick({
                    dots: true,
                    arrows: false,
                    fade: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    autoplay: true,
                    autoplaySpeed: 2000
                });
            } else {
                this.destroySlider();
            }
        }

        /**
         * События слайдера форума
         */

    }, {
        key: 'bindForumEvents',
        value: function bindForumEvents() {
            var that = this;

            $(window).ready(that.initForumSlider());
            $(window).resize(function () {
                that.initForumSlider();
            });
        }

        /**
         * Инициализация слайдера партнеров
         */

    }, {
        key: 'initPartnersSlider',
        value: function initPartnersSlider() {
            this.$slider.slick({
                dots: false,
                arrows: true,
                slidesToShow: 7,
                slidesToScroll: 1,
                autoplay: false,
                responsive: [{
                    breakpoint: 1440,
                    settings: {
                        slidesToShow: 6,
                        infinite: true,
                        draggable: true,
                        swipe: true,
                        autoplay: false
                    }
                }, {
                    breakpoint: 900,
                    settings: {
                        slidesToShow: 4
                    }
                }, {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        draggable: false,
                        swipe: false,
                        autoplay: true,
                        autoplaySpeed: 2000
                    }
                }]
            });
        }

        /**
         * Удалить слайдер
         */

    }, {
        key: 'destroySlider',
        value: function destroySlider() {
            if (this.$slider[0].slick) {
                this.$slider.slick('unslick');
            }
        }
    }]);

    return Slider;
}();

var Submenu = function () {
    function Submenu($menu) {
        _classCallCheck(this, Submenu);

        this.$menu = $menu;
        this.$menuOpenLink = $menu.find('.js-submenu-open');
        this.$menuCloseLink = $menu.find('.js-submenu-close');
        this.$menuBody = $menu.find('.js-submenu-body');
        this.asideMenu = $('.js-scroll');

        this.bindEvents();
    }

    /**
     * События
     */


    _createClass(Submenu, [{
        key: 'bindEvents',
        value: function bindEvents() {
            var _this7 = this;

            this.$menuOpenLink.on('click', function (e) {
                e.preventDefault();
                _this7.$menuBody.addClass('is-open');
                _this7.$menuOpenLink.addClass('is-hidden');
                _this7.asideMenu.addClass('is-hidden');
            });

            this.$menuCloseLink.on('click', function (e) {
                e.preventDefault();
                _this7.$menuBody.removeClass('is-open');
                _this7.$menuOpenLink.removeClass('is-hidden');
                _this7.asideMenu.removeClass('is-hidden');
            });
        }
    }]);

    return Submenu;
}();

var Scroll = function () {
    function Scroll($main) {
        _classCallCheck(this, Scroll);

        this.$mainContent = $main;
        this.$sections = $main.find('.js-section');
        this.$burger = $('.js-submenu-open');
        this.$header = $('.js-header');
        this.$headerItems = this.$header.find('.js-header-item');
        this.$asideMenu = $('.js-scroll');
        this.$asideMenuItems = this.$asideMenu.find('li');
        this.$asidePageName = this.$asideMenu.find('.js-menu-page');
        this.$headerLogo = $('.b-header__logo');
    }

    /**
     * Инициализация вертикального скролла
     */


    _createClass(Scroll, [{
        key: 'initVerticalScroll',
        value: function initVerticalScroll() {
            var that = this;

            $('#fullpage').fullpage({
                anchors: ['section_0', 'section_1', 'section_2', 'section_3', 'section_4', 'section_5', 'section_6', 'section_7', 'section_8'],
                menu: '#js-menu',
                sectionSelector: '.js-section',
                afterLoad: function afterLoad(anchorLink, index) {
                    if (index === 1) {
                        that.$asideMenu.addClass('is-first');
                        that.$header.hide();
                    }
                },
                onLeave: function onLeave(index, nextIndex, direction) {
                    var $curPage = $(that.$headerItems[nextIndex - 2]);

                    that.$headerItems.removeClass('active');
                    $curPage.addClass('active');

                    if (nextIndex === 1) {
                        that.$asideMenu.addClass('is-first').removeClass('is-white');
                        that.$headerLogo.removeClass('is-white');
                    } else {
                        that.$asideMenu.removeClass('is-first').removeClass('is-white');
                        that.$header.show();
                        that.$headerLogo.removeClass('is-white');
                    }

                    if (nextIndex === that.$sections.length) {
                        that.$asideMenu.addClass('is-white');
                        that.$headerLogo.addClass('is-white');
                    }
                }
            });
        }

        /**
         * Инициализация горизонтального скролла
         */

    }, {
        key: 'initHorizontalScroll',
        value: function initHorizontalScroll() {
            var that = this;

            $('#fullpage').fullpage({
                anchors: ['section_0', 'section_1', 'section_2', 'section_3', 'section_4', 'section_5', 'section_6', 'section_7', 'section_8'],
                menu: '#js-menu',
                controlArrows: false,
                loopHorizontal: false,
                sectionSelector: '.js-main-section',
                slideSelector: '.js-section',
                fixedElements: '.js-scroll',
                autoScrolling: false,
                css3: false,
                afterLoad: function afterLoad(anchorLink, index) {
                    if (index === 1) {
                        that.$header.hide();
                        that.$burger.addClass('is-white');
                    }
                    if (index === that.$sections.length) {
                        that.$header.addClass('is-black');
                    }
                },
                onSlideLeave: function onSlideLeave(anchorLink, index, slideIndex, direction, nextSlideIndex) {
                    var $nextPage = $(that.$asideMenuItems[nextSlideIndex]),
                        pageText = $nextPage.find('.js-page-name').text();

                    that.$asideMenuItems.removeClass('active');
                    $nextPage.addClass('active');
                    that.$asidePageName.text(pageText);

                    if (nextSlideIndex === 0) {
                        that.$header.hide();
                        $nextPage.addClass('is-first');
                        that.$burger.addClass('is-white');
                        that.$asidePageName.addClass('is-first');
                        that.$asideMenu.removeClass('is-white');
                        that.$header.removeClass('is-black');
                        that.$headerLogo.removeClass('is-white');
                    } else {
                        that.$header.show();
                        $nextPage.removeClass('is-first');
                        that.$burger.removeClass('is-white');
                        that.$asidePageName.removeClass('is-first');
                        that.$asideMenu.removeClass('is-white');
                        that.$header.removeClass('is-black');
                        that.$headerLogo.removeClass('is-white');
                    }

                    if (nextSlideIndex === that.$sections.length - 1) {
                        that.$asideMenu.addClass('is-white');
                        that.$header.addClass('is-black');
                        that.$headerLogo.addClass('is-white');
                    }
                }
            });
        }

        /**
         * Удалить объект fullpage.js
         */

    }, {
        key: 'reinitScroll',
        value: function reinitScroll() {
            if ($('.fp-enabled').length) {
                $.fn.fullpage.destroy('all');
            }
        }
    }]);

    return Scroll;
}();

$(function () {
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
    var $body = $('body');
    var isLanding = $('.js-main').length;

    //Перестановка блоков для слайдера экспертов
    (function () {
        if (isLanding) {
            $(window).ready(permutation());
            $(window).resize(function () {
                permutation();
            });
        }
    })($(window));

    // Класс для ИЕ
    (function () {
        if (checkIe()) {
            $body.addClass('is-ie');
        }
        if (checkIOS()) {
            $body.addClass('is-ios');
        }
    })($body);

    // фикс object-fit для ие
    (function ($imgs) {
        if (checkIe()) {

            if (!$imgs.length) {
                return;
            }

            $imgs.each(function () {
                var $container = $(this),
                    imgUrl = $container.find('img').prop('src');

                if (imgUrl) {
                    $container.css('backgroundImage', 'url(' + imgUrl + ')').addClass('ie-object-fit');
                }
            });
        }
    })($('.js-img-fit'));

    //Подключение постраничного скролла
    (function ($main) {
        if (!$main.length) {
            return;
        }

        var scroll = new Scroll($main);
        if (isMobile) {
            scroll.initHorizontalScroll();
        } else {
            scroll.initVerticalScroll();
        }
    })($('.js-main'));

    //Подключение выпадающего меню
    (function ($menu) {
        if (!$menu.length) {
            return;
        }

        var menu = new Submenu($menu);
    })($('.js-submenu'));

    //Подключение слайдера
    (function ($sliders) {
        if (!$sliders.length) {
            return;
        }
        $sliders.each(function () {
            var slider = new Slider($(this));
        });
    })($('.js-slider'));

    //Подключение попапа
    (function ($popup) {
        if (!$popup.length) {
            return;
        }
        $popup.each(function () {
            var popup = new Popup($(this));
        });
    })($('.js-popup'));

    //Подключение блока стать участником
    (function ($part) {
        if (!$part.length) {
            return;
        }
        var participant = new Participant($part);
    })($('.js-part'));

    //Подключение тултипов
    (function ($tooltips) {
        if (!$tooltips.length) {
            return;
        }
        $tooltips.each(function () {
            var tooltip = new Tooltip($(this));
        });
    })($('.js-tooltip-link'));

    //Подключение валидации формы
    (function ($forms) {
        if (!$forms.length) {
            return;
        }

        var form = new Validation();
    })($('.js-form-validation'));

    //Подключение табов
    (function ($tabs) {
        if (!$tabs.length) {
            return;
        }
        $tabs.each(function () {
            var tab = new Tab($(this));
        });
    })($('.js-tab'));

    //Инициализация карты
    (function ($maps) {
        if (!$maps.length) {
            return;
        }

        $maps.each(function () {
            return new Map($(this));
        });
    })($('.js-map'));

    //Тень в конце текста
    (function ($shadows) {
        if (!$shadows.length) {
            return;
        }

        $shadows.each(function () {
            return new Shadow($(this));
        });
    })($('.js-shadow'));

    //Подключение табов
    (function ($selects) {
        if (!$selects.length) {
            return;
        }
        $selects.each(function () {
            var select = new Select($(this));
        });
    })($('.js-select'));

    //Подключение svg-карты
    (function ($map) {
        if (!$map.length) {
            return;
        }
        var map = new MapSvg($map);
    })($('.js-svg-map'));

    //Подключение dropdown
    (function ($dropdowns) {
        if (!$dropdowns.length) {
            return;
        }
        $dropdowns.each(function () {
            var dropdown = new Dropdown($(this));
        });
    })($('.js-dropdown'));

    //Подключение lightbox
    (function ($lightboxs) {
        if (!$lightboxs.length) {
            return;
        }
        $lightboxs.each(function () {
            var lightbox = new Lightbox($(this));
        });
    })($('.js-lightbox'));

    //Подключение маски
    (function ($masks) {
        if (!$masks.length) {
            return;
        }

        var mask = new Mask();
        mask.initPhoneMask();
    })($('.js-phone-mask'));

    //Подключение фильтра по программе
    (function ($filter) {
        if (!$filter.length) {
            return;
        }

        var filter = new ProgramFilter($filter);
    })($('.js-program-filter'));
});

function checkIe() {
    var msIe = false;
    var ua = window.navigator.userAgent;
    var oldIe = ua.indexOf('MSIE ');
    var newIe = ua.indexOf('Trident/');

    if (oldIe > -1 || newIe > -1) {
        msIe = true;
    }

    return msIe;
}

function checkIOS() {
    var isIOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    return isIOS;
}

function permutation() {
    var $controls = $('.js-permutation-controls'),
        $btn = $('.js-permutation-btn');

    if ($(window).width() > 768) {
        $controls.detach().appendTo('.b-expert__slider-controls');
        $btn.detach().appendTo('.b-expert__item--photo');
    } else {
        $controls.detach().insertAfter('.b-expert__title');
        $btn.detach().insertAfter('.b-expert__item--text');
    }
}
$('.b-hotels-filter__table tbody tr[data-href]').addClass('clickable').click(function () {
    window.location = $(this).attr('data-href');
});