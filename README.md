## Перед сборкой нужно установить
node.js - https://nodejs.org/ + npm

## Установить gulp 
```
$ npm install -g bower
```

## Установить bower 
```
$ npm install -g bower
 ```

## Установить плагины из package.json
```
$ npm i
```

## Установить доп.библиотеки из bower.json
```
$ bower i
```

## Важно 
1. Установка доп.плагинов и библиотек происходит с ключами --save --dev
2. Использовать по возможности bower
3. Библиотеки устанавливаются в папку src/libs - изменить путь можно в файле .bowerrc


## Сборка
Сборка проекта через команды:
```
$ gulp dev
$ gulp build
```


## Дополнительное

**gulp-file-include** - используется для "шаблонизации", позволяет импортировать один файл в другой с передачей каких-либо параметров <br/>
см. https://www.npmjs.com/package/gulp-file-include


**gulp-svg-sprites** - генерит svg спрайт из файлов /src/images/svgSprite/*.svg <br/>
см. https://github.com/shakyshane/gulp-svg-sprites

!Важно называть файлы правильно и осмысленно т.к. из имени файла бедет сгенерен id.

Использование через миксин icon.jade:
```
+icon('belt')
```

Про свг-спрайты: http://dreamhelg.ru/2017/02/symbol-svg-sprite-detail-guide/


**gulp-eslint** - статический анализатор кода, файл с правилами: .eslintrc <br/>
см. https://github.com/adametry/gulp-eslint 