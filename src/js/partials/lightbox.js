class Lightbox {
    constructor($lightbox) {
        this.$lightbox = $lightbox;
        this.$mainThumb = $lightbox.find('.js-lightbox-main');
        this.$thumbs = $lightbox.find('.js-lightbox-thumb');
        this.$mainImg= this.$mainThumb.find('img');
        this.$mainLink= this.$mainThumb.find('a');

        this.bindEvents();
    }

    /**
     * События
     */
    bindEvents() {
        let that = this,
            mainImgSrc = this.$mainImg[0].src;
        
        this.$thumbs.each(function() {
            let $thumb = $(this);

            $thumb.on('click', (e) => {
                let $this = $(this),
                    $img = $this.find('img'),
                    imgSrc = $img[0].src;

                e.preventDefault();

                that.$mainImg[0].src = imgSrc;
                that.$mainLink.attr('href', imgSrc);

                $img[0].src = mainImgSrc;
                mainImgSrc = imgSrc;
            });
        });
    }
}