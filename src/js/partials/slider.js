class Slider {
    constructor($slider) {
        this.$slider = $slider;
        this.sliderType = this.$slider.data('type');

        if ( this.sliderType === 'expert') {
            this.initExpertSlider();
            this.bindExpertEvents();
        } else if ( this.sliderType === 'forum') {
            this.bindForumEvents();
        } else if ( this.sliderType === 'partners') {
            this.initPartnersSlider();
        } else {
            this.initSlider();
        }
    }

    /**
     * Инициализация стандартного слайдера
     */
    initSlider() {
        this.$slider.slick({
            dots: true,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            infinite: true,
            draggable: false,
            swipe: false
          });
    }

    /**
     * Инициализация слайдера экспертов
     */
    initExpertSlider() {
        let $sliderSync = $('.js-slider-sync');

        this.$slider.slick({
            dots: false,
            arrows: false,
            fade: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: '.js-experts',
            draggable: false,
            swipe: false
        });

        $sliderSync.slick({
            dots: false,
            arrows: false,
            slidesToShow: 5,
            responsive: [
              {
                breakpoint: 1440,
                settings: {
                    slidesToShow: 3,
                }
              }
            ],
            slidesToScroll: 1,
            asNavFor: '.js-expert',
            centerMode: true,
            focusOnSelect: true,
            draggable: false,
            swipe: false
        });
    }

    /**
     * События слайдера экспертов
     */
    bindExpertEvents() {   
        let $sliderSync = $('.js-slider-sync'),
            $sliderNext = this.$slider.closest('.js-slider-experts').find('.js-slider-next'),
            $sliderPrev = this.$slider.closest('.js-slider-experts').find('.js-slider-prev');

        $sliderNext.on('click', (e) => {
            $sliderSync.slick('slickNext');
        });

        $sliderPrev.on('click', (e) => {
            $sliderSync.slick('slickPrev');
        });
    }

    /**
     * Инициализация слайдера форума
     */
    initForumSlider() {
        if ($(window).width() < 960) {
            this.destroySlider();
            this.$slider.slick({
                dots: true,
                arrows: false,
                fade: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 2000,
            });
        } else {            
            this.destroySlider();
        }
    }

    /**
     * События слайдера форума
     */
    bindForumEvents() {
        let that = this;

        $(window).ready(that.initForumSlider());
        $(window).resize(function() {
            that.initForumSlider();
        });
    }

    /**
     * Инициализация слайдера партнеров
     */
    initPartnersSlider() {
        this.$slider.slick({
            dots: false,
            arrows: true,
            slidesToShow: 7,
            slidesToScroll: 1,
            autoplay: false,
            responsive: [
                {
                  breakpoint: 1440,
                  settings: {
                    slidesToShow: 6,
                    infinite: true,
                    draggable: true,
                    swipe: true,
                    autoplay: false,
                  }
                },
                {
                  breakpoint: 900,
                  settings: {
                    slidesToShow: 4
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                    draggable: false,
                    swipe: false,
                    autoplay: true,
                    autoplaySpeed: 2000,
                  }
                }
              ]
          });
    }

    /**
     * Удалить слайдер
     */
    destroySlider() {
        if (this.$slider[0].slick) {
            this.$slider.slick('unslick');
        }
    }
}