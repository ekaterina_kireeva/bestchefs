class Select {
    constructor($select) {
        this.$select = $select.find('.js-select-object');
        this.$selectTarget = $select.find('.js-select-target');
        this.$selectHead = this.$select.find('.js-select-head');
        this.$selectLinks = this.$select.find('.js-select-link');

        this.bindEvents();
    }

    /**
     * События
     */
    bindEvents() {
        let that = this;

        this.$selectHead.on('click', (e) => {
            e.preventDefault();
            that.$select.toggleClass('is-active');
        });

        this.$selectLinks.each(function() {
            let $link = $(this),
                $target = $($link.attr('href'));

            $link.on('click', (e) => {
                e.preventDefault();

                that.$selectTarget.removeClass('is-active').hide();
                $target.addClass('is-active').fadeIn();

                that.$selectHead.text($link.text());
                that.$select.removeClass('is-active');
                that.$selectLinks.removeClass('is-hidden');
                $link.addClass('is-hidden');
            });
        });

        $(document).mouseup((e) => {
            if (!this.$select.is(e.target) && this.$select.has(e.target).length === 0) {
                this.$select.removeClass('is-active');
            }
        });
    }
}