class MapSvg {
    constructor($map) {
        this.$map = $map;
        this.$mapRegions = $map.find('.js-svg-fo');
        this.$popup = $map.find('.js-svg-popup');

        this.bindEvents();
    }

    /**
     * События
     */
    bindEvents() {
        let that = this;

        this.$mapRegions.each(function() {
            let $region = $(this);

            $region.on('click', (e) => {
                let $this = $(this),
                    foId = $this.attr('id'),
                    dataUrl = $this.data('url');
            
                that.$popup.removeClass('is-active');

                $.ajax({
                    url: dataUrl,
                    data: {
                        fo: foId
                    },
                    success: function (data) {
                        that.$popup.html(data);
                        that.updatePopup();
                    }
                })

                that.$popup.addClass('is-active');
                $this.addClass('is-active');
            });
        })

        $('body').on('click', '.js-svg-close', (e) => {
            that.$popup.removeClass('is-active');
            that.$mapRegions.removeClass('is-active');
        });

        $(document).mouseup((e) => {
            if (!that.$popup.is(e.target) && that.$popup.has(e.target).length === 0) {
                that.$popup.removeClass('is-active');
                that.$mapRegions.removeClass('is-active');
            }
        });
    }

    /**
     * Обновление поп-апа после ajax запроса
     */
    updatePopup() {
        this.$popup = this.$map.find('.js-svg-popup');
    }
} 