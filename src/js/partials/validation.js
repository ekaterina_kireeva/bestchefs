class Validation {
    constructor() {
        this.bindEvents();
    }

    /**
     * События
     */
    bindEvents() {
        let that = this;

        $('body').on('click', '.js-btn-submit', (e) => {
            let isValid = true,
                $thisBtn = $(e.currentTarget),
                $form = $thisBtn.closest('form'),
                $fields = $form.find('[data-valid]');

            that.clearFormError($form);

            $fields.each(function() {
                let $this = $(this),
                    type = $(this).data('type');

                if (type === 'text') {
                    if ( !that.checkText($this) ) {
                        that.setError($this);
                        isValid = false;
                    }
                } 

                if (type === 'phone') {
                    if ( !that.checkPhone($this) ) {
                        that.setError($this);
                        isValid = false;
                    }
                } 

                if (type === 'email') {
                    if ( !that.checkEmail($this) ) {
                        that.setError($this);
                        isValid = false;
                    }
                }
            });

            if (!isValid) {
                e.preventDefault();
            } else {
                let url = $form.attr('action'),
                    data = $form.serializeArray(),
                    $popup = $form.closest('.b-popup');

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    success: function success(data) {
                        let mask = new Mask();
                        $popup.html(data);
                        mask.initPhoneMask();
                    },
                    error: function error(data) {
                        $popup.html(data);
                    }
                })
            }
        });
    }

    /**
     * Проверка текстового поля
     */
    checkText($field) {
        return $field.val().length > 0;
    }

    /**
     * Проверка телефона
     */
    checkPhone($field) {
        let regex = /^((8|\+7)[\-]?)?(\(?\d{3}\)?[\-]?)?[\d\-]{7}$/;
        return regex.test($field.val());
    }

    /**
     * Проверка email
     */
    checkEmail($field) {
        let regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/;
        return regex.test($field.val());
    }

    /**
     * Добавить полю ошибку
     */
    setError($field) {
        $field.addClass('is-error');
    }

    /**
     * Очистить форму от ошибок
     */
    clearFormError($form) {
        $form.find('.is-error').removeClass('is-error');
    }
}