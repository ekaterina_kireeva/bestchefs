class ProgramFilter {
    constructor($filter) {
        this.$filter = $filter;

        this.bindEvents();
    }

    /**
     * События
     */
    bindEvents() {
        this.checkInCart();
        this.openTooltip();
    }

    /**
     * Изменения состояния чекбокса в корзину
     */
    checkInCart() {
        $('body').on('click', '.js-in-cart', function(e) {
            let $this = $(e.currentTarget),
                $input = $this.siblings('input'),
                $txt = $this.find('.js-in-cart-txt'),
                isClicked = !($input.prop('checked'));
            
            (isClicked) ? $txt.text('входит') : $txt.text('в корзину');
        });
    }

    /**
     * Открыть тултип со списком программ
     */
    openTooltip() {
        $('body').on('click', '.js-open-tooltip', function(e) {
            let $this = $(e.currentTarget),
                $tooltip = $this.siblings('.js-programs-tooltip'),
                $parentLi = $this.closest('li'),
                $allLink = $parentLi.siblings('li').find('a');

            e.preventDefault();
            $this.addClass('is-active');
            ($this.hasClass('is-active')) ? $tooltip.show() : $tooltip.hide();

            $(document).mouseup((e) => {
                if (!$tooltip.is(e.target) && $tooltip.has(e.target).length === 0) {
                    if ($allLink.hasClass('is-active')) {
                        $this.removeClass('is-active');
                    }
                    $tooltip.hide();
                }
            });
        });
    }
}