class Popup {
    constructor($popup) {
        this.$popup = $popup;
        this.type = $popup.data('type');

        if(this.type === 'img') {
            this.initImgPopup();
        } else {
            this.initPopup();
        }
        this.bindEvents();
    }

    /**
     * События
     */
    bindEvents() {
        $('body').on('click','.js-popup-close', this.hidePopup);
    }

    /**
     * Инициализация попапа
     */
    initPopup() {
        let that = this;
        
        this.$popup.magnificPopup({
            type : 'inline',
            showCloseBtn   : false,
            fixedContentPos: true,
            callbacks      : {
                open: function() {
                    that.openPopup(this.content);
                }
            } 
        });
    }

    /**
     * Инициализация галлерии
     */
    initImgPopup() {
        let that = this;
        
        this.$popup.magnificPopup({
            type : 'image',
            closeOnContentClick: true,
            showCloseBtn   : true,
            fixedContentPos: true,
            image: {
                verticalFit: true
            },
            callbacks      : {
                open: function() {
                    that.openPopup(this.content);
                }
            } 
        });
    }

    /**
     * Событие открытия
     */
    openPopup(popup) {
        let $popup = popup;
        
        if (this.type === 'buy') {
            $popup.parent().addClass('tickets-popup');
        }

        if (this.type === 'video') {
            $popup.parent().addClass('video-popup');
        }

        if (this.type === 'form') {
            $popup.parent().addClass('form-popup');
        }

        if (this.type === 'img') {
            $popup.parent().addClass('img-popup');
        }
    }

    /**
     * Закрытие попапа
     */
    hidePopup() {
        $.magnificPopup.close();
    }
}