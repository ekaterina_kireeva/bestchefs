class Tab {
    constructor($tab) {
        this.$tab = $tab;
        this.$tabLinks = this.$tab.find('.js-tab-link');
        this.$tabTargets = this.$tab.find('.js-tab-target');
        
        this.bindEvents();
    }

    /**
     * События
     */
    bindEvents() {
        let that = this;

        this.$tabLinks.each(function() {
            let $link = $(this),
                $target = $($link.attr('href'));

            $link.on('click', (e) => {
                e.preventDefault();
                that.$tabLinks.removeClass('is-active');
                $link.addClass('is-active');
                that.$tabTargets.removeClass('is-active').hide();
                $target.addClass('is-active').fadeIn();
            });
        });
    }
}