class Mask {

    /**
     * Маска для номера телефона
     */
    initPhoneMask() {
        $(".js-phone-mask").each(function() {
            $(this).mask("+7 (999) 999-99-99", {autoclear: false});
        });
    }
}