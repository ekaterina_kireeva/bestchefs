class Tooltip {
    constructor($tooltipLink) {
        this.$tooltipLink = $tooltipLink;
        this.$tooltip = this.$tooltipLink.closest('.js-tooltip');
        this.$tooltipBody = this.$tooltip.find('.js-tooltip-body');
        this.$tooltipClose = this.$tooltip.find('.js-tooltip-close');

        this.bindEvents();
    }

    /**
     * События
     */
    bindEvents() {
        this.$tooltipLink.on('click', (e) => {
            if ($(window).width() > 1020) {
                e.preventDefault();
                this.$tooltipBody.attr('style', '');

                let windowWidth = $(window).width(),
                    linkWidth = this.$tooltipLink.outerWidth(),
                    linkLeft = this.$tooltipLink.offset().left,

                    right = linkLeft + linkWidth,
                    tooltipWidth = this.$tooltipBody.outerWidth();

                if( windowWidth - right < tooltipWidth) {
                    let newWidth = windowWidth - right;
                    this.$tooltipBody.css({
                        'min-width': newWidth + 'px',
                        'width': newWidth + 'px'
                    })
                }
                this.$tooltip.addClass('is-active');
            }
        });

        this.$tooltipClose.on('click', (e) => {
            this.$tooltip.removeClass('is-active');
        });

        $(document).mouseup((e) => {
            if (!this.$tooltip.is(e.target) && this.$tooltip.has(e.target).length === 0) {
                this.$tooltip.removeClass('is-active');
            }
        });
    }
}