class Participant {
    constructor($partLink) {
        this.$partLink = $partLink;
        this.$partBody = $($partLink.attr('href'));
        this.$menu = $('.js-menu');
        this.$menuRows = this.$menu.find('.js-menu-row');

        this.bindEvents();
    }

    /**
     * События
     */
    bindEvents() {
        let isOpen = false;

        this.$partLink.on('click', (e) => {
            let windowWidth = $(window).width();

            e.preventDefault();
            (isOpen) ? isOpen = false : isOpen = true;

            (isOpen) ? this.$partBody.fadeIn() : this.$partBody.hide();
            this.$partLink.toggleClass('is-open');

            if (windowWidth >= 1280) {
                this.$menuRows.last().toggleClass('is-hidden');
            } else {
                this.$menu.toggleClass('is-open');          
                this.$menuRows.toggleClass('is-hidden');
            }
        });
    }
}