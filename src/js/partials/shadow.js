class Shadow {
    constructor($shadowBlock) {
        this.$shadowBlock = $shadowBlock;

        this.checkIsShadow();
        this.bindEvents();
    }

    /**
     * События
     */
    bindEvents() {
        let that = this;

        this.$shadowBlock.on('scroll', (e)=> {
            that.$shadowBlock.removeClass('is-scroll-end');
            that.checkIsShadow();
        });
    }

    /**
     * Нужна ли тень
     */
    checkIsShadow(isFirstCall) {
        let that = this,
            scrollPosition = that.$shadowBlock.scrollTop() + that.$shadowBlock.height(),
            scrollHeight = that.$shadowBlock[0].scrollHeight;

        if (scrollPosition >= scrollHeight ) {
            that.$shadowBlock.addClass('is-scroll-end');
        }
    }
}