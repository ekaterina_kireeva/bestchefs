$(function() {
    let isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;
    let $body = $('body');
    let isLanding = $('.js-main').length;

    //Перестановка блоков для слайдера экспертов
    (function() {
        if (isLanding) {
            $(window).ready(permutation());
            $(window).resize(function() { 
                permutation();
            });
        }
    })($(window));

    // Класс для ИЕ
    (function() {
        if (checkIe()) {
            $body.addClass('is-ie');
        }
        if (checkIOS()) {
            $body.addClass('is-ios');
        }
    })($body);

    // фикс object-fit для ие
    (function($imgs) {
        if (checkIe()) {

            if (!$imgs.length) {
                return;
            }

            $imgs.each(function() {
                let $container = $(this),
                    imgUrl = $container.find('img').prop('src');

                if(imgUrl) {
                    $container.css('backgroundImage', 'url(' + imgUrl + ')')
                        .addClass('ie-object-fit');
                }
            });
        }
    })($('.js-img-fit'));

    //Подключение постраничного скролла
    (function($main) {
        if (!$main.length) {
            return;
        }

        let scroll = new Scroll($main);
        if(isMobile) {
            scroll.initHorizontalScroll();
        } else {
            scroll.initVerticalScroll();            
        }

    })($('.js-main'));

    //Подключение выпадающего меню
    (function($menu) {
        if (!$menu.length) {
            return;
        }

        let menu = new Submenu($menu);
    })($('.js-submenu'));

    //Подключение слайдера
    (function($sliders) {
        if (!$sliders.length) {
            return;
        }
        $sliders.each(function() {
            let slider = new Slider($(this));
        });
    })($('.js-slider'));
    
    //Подключение попапа
    (function($popup) {
        if (!$popup.length) {
            return;
        }
        $popup.each(function() {
            let popup = new Popup($(this));
        });
    })($('.js-popup'));

    //Подключение блока стать участником
    (function($part) {
        if (!$part.length) {
            return;
        }
        let participant = new Participant($part);
    })($('.js-part'));

    //Подключение тултипов
    (function($tooltips) {
        if (!$tooltips.length) {
            return;
        }
        $tooltips.each(function() {
            let tooltip = new Tooltip($(this));
        });
    })($('.js-tooltip-link'));

    //Подключение валидации формы
    (function($forms) {
        if (!$forms.length) {
            return;
        }
        
        let form = new Validation();
    })($('.js-form-validation'));

    //Подключение табов
    (function($tabs) {
        if (!$tabs.length) {
            return;
        }
        $tabs.each(function() {
            let tab = new Tab($(this));
        });
    })($('.js-tab'));
    
    //Инициализация карты
    (function($maps) {
        if (!$maps.length) {
            return;
        }

        $maps.each(function() {
            return new Map($(this));
        });
    })($('.js-map'));

    //Тень в конце текста
    (function($shadows) {
        if (!$shadows.length) {
            return;
        }

        $shadows.each(function() {
            return new Shadow($(this));
        });
    })($('.js-shadow'));
    
    //Подключение табов
    (function($selects) {
        if (!$selects.length) {
            return;
        }
        $selects.each(function() {
            let select = new Select($(this));
        });
    })($('.js-select'));
    
    //Подключение svg-карты
    (function($map) {
        if (!$map.length) {
            return;
        }
        let map = new MapSvg($map);
    })($('.js-svg-map'));
    
    //Подключение dropdown
    (function($dropdowns) {
        if (!$dropdowns.length) {
            return;
        }
        $dropdowns.each(function() {
            let dropdown = new Dropdown($(this));
        });
    })($('.js-dropdown'));
    
    //Подключение lightbox
    (function($lightboxs) {
        if (!$lightboxs.length) {
            return;
        }
        $lightboxs.each(function() {
            let lightbox = new Lightbox($(this));
        });
    })($('.js-lightbox'));
    
    //Подключение маски
    (function($masks) {
        if (!$masks.length) {
            return;
        }

        let mask = new Mask();
            mask.initPhoneMask();
    })($('.js-phone-mask'));

    //Подключение фильтра по программе
    (function($filter) {
        if (!$filter.length) {
            return;
        }

        let filter = new ProgramFilter($filter);
    })($('.js-program-filter'));
});

function checkIe() {
    let msIe = false;
    const ua = window.navigator.userAgent;
    const oldIe = ua.indexOf('MSIE ');
    const newIe = ua.indexOf('Trident/');

    if (oldIe > -1 || newIe > -1) {
        msIe = true;
    }

    return msIe;
}

function checkIOS() {
    let isIOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    return isIOS;
}

function permutation() {
    let $controls = $('.js-permutation-controls'),
        $btn = $('.js-permutation-btn');

    if ($(window).width() > 768) {
        $controls.detach().appendTo('.b-expert__slider-controls');
        $btn.detach().appendTo('.b-expert__item--photo');
    } else {
        $controls.detach().insertAfter('.b-expert__title');
        $btn.detach().insertAfter('.b-expert__item--text');
    }
}