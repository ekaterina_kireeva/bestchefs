class Map {
    constructor($map) {
        this.$map = $map;
        this.mapId = this.$map.attr('id');
        this.map = {};

        this.initMap();
    }

    /**
     * Инициализация карты
     */
    initMap() {
        ymaps.ready(() => {
            this.map = new ymaps.Map(this.mapId, {
                center: [55.76, 37.64], 
                zoom: 7
            });
        });
    }
}