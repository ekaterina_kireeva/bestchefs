class Submenu {
    constructor($menu) {
        this.$menu = $menu;
        this.$menuOpenLink = $menu.find('.js-submenu-open');
        this.$menuCloseLink = $menu.find('.js-submenu-close');
        this.$menuBody = $menu.find('.js-submenu-body');
        this.asideMenu = $('.js-scroll');

        this.bindEvents();
    }

    /**
     * События
     */
    bindEvents() {
        this.$menuOpenLink.on('click', (e) => {
            e.preventDefault();
            this.$menuBody.addClass('is-open');
            this.$menuOpenLink.addClass('is-hidden');
            this.asideMenu.addClass('is-hidden');
        });

        this.$menuCloseLink.on('click', (e) => {
            e.preventDefault();
            this.$menuBody.removeClass('is-open');
            this.$menuOpenLink.removeClass('is-hidden');
            this.asideMenu.removeClass('is-hidden');
        });
    }
}