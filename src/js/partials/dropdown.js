class Dropdown {
    constructor($dropdown) {
        this.$dropdown = $dropdown;
        this.$dropdownBody = $dropdown.find('.js-dropdown-body');

        this.bindEvents();
    }

    /**
     * События
     */
    bindEvents() {
        let that = this;

        this.$dropdown.on('click', (e) => {
            that.$dropdown.toggleClass('is-active');
            that.$dropdownBody.toggleClass('is-active');
        });

        $(document).mouseup((e) => {
            if (!that.$dropdown.is(e.target) && that.$dropdown.has(e.target).length === 0) {
                that.$dropdown.removeClass('is-active');
                that.$dropdownBody.removeClass('is-active');
            }
        });
    }
}