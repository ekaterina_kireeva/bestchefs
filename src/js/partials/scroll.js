class Scroll {
    constructor($main) {
        this.$mainContent = $main;
        this.$sections = $main.find('.js-section');
        this.$burger = $('.js-submenu-open');
        this.$header = $('.js-header');
        this.$headerItems = this.$header.find('.js-header-item');
        this.$asideMenu = $('.js-scroll');
        this.$asideMenuItems = this.$asideMenu.find('li');
        this.$asidePageName = this.$asideMenu.find('.js-menu-page');
        this.$headerLogo = $('.b-header__logo');
    }

    /**
     * Инициализация вертикального скролла
     */
    initVerticalScroll() {
        let that = this;

        $('#fullpage').fullpage({
            anchors: ['section_0', 'section_1', 'section_2', 'section_3',
                'section_4','section_5','section_6','section_7','section_8'],
            menu: '#js-menu',
            sectionSelector: '.js-section',
            afterLoad: function(anchorLink, index) {
                if (index === 1) {
                    that.$asideMenu.addClass('is-first');
                    that.$header.hide();
                }
            },
            onLeave: function(index, nextIndex, direction) {
                let $curPage = $(that.$headerItems[nextIndex-2]);

                that.$headerItems.removeClass('active');
                $curPage.addClass('active');

                if (nextIndex === 1) {
                    that.$asideMenu.addClass('is-first').removeClass('is-white');
                    that.$headerLogo.removeClass('is-white');
                } else {
                    that.$asideMenu.removeClass('is-first').removeClass('is-white');
                    that.$header.show();
                    that.$headerLogo.removeClass('is-white');
                }

                if (nextIndex === that.$sections.length) {
                    that.$asideMenu.addClass('is-white');
                    that.$headerLogo.addClass('is-white');
                }
            }
        });
    }

    /**
     * Инициализация горизонтального скролла
     */
    initHorizontalScroll() {
        let that = this;

        $('#fullpage').fullpage({
            anchors: ['section_0', 'section_1', 'section_2', 'section_3',
                'section_4','section_5','section_6','section_7','section_8'],
            menu: '#js-menu',
            controlArrows: false,
            loopHorizontal: false,
            sectionSelector: '.js-main-section',
            slideSelector: '.js-section',
            fixedElements: '.js-scroll',
            autoScrolling: false,
            css3:false,
            afterLoad: function(anchorLink, index) {
                if (index === 1) {
                    that.$header.hide();
                    that.$burger.addClass('is-white');
                }
                if (index === that.$sections.length) {
                    that.$header.addClass('is-black');
                }
            },
            onSlideLeave: function(anchorLink, index, slideIndex, direction, nextSlideIndex) {
                let $nextPage = $(that.$asideMenuItems[nextSlideIndex]),
                    pageText = $nextPage.find('.js-page-name').text();

                that.$asideMenuItems.removeClass('active');
                $nextPage.addClass('active');
                that.$asidePageName.text(pageText);

                if (nextSlideIndex === 0) {
                    that.$header.hide();
                    $nextPage.addClass('is-first');
                    that.$burger.addClass('is-white');
                    that.$asidePageName.addClass('is-first');
                    that.$asideMenu.removeClass('is-white');
                    that.$header.removeClass('is-black');
                    that.$headerLogo.removeClass('is-white');
                } else {
                    that.$header.show();
                    $nextPage.removeClass('is-first');
                    that.$burger.removeClass('is-white');
                    that.$asidePageName.removeClass('is-first');
                    that.$asideMenu.removeClass('is-white');
                    that.$header.removeClass('is-black');
                    that.$headerLogo.removeClass('is-white');
                }

                if (nextSlideIndex === that.$sections.length - 1) {
                    that.$asideMenu.addClass('is-white');
                    that.$header.addClass('is-black');
                    that.$headerLogo.addClass('is-white');
                }
            }
        });
    }


    /**
     * Удалить объект fullpage.js
     */
    reinitScroll() {
        if ( $('.fp-enabled').length ) {
            $.fn.fullpage.destroy('all');
        }
    }
}