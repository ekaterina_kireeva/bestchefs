'use strict'
const gulp = require('gulp'), //подключаем Gulp к проекту

      build = './build/',
      base = './src/';

var path = {
        html: {
            src: base + '**/*.pug',
            dest: build
        },
        css: {
            styleInternal:  base + 'style/internal.scss',
            styleExternal: base + 'style/external.scss',
            
            dest: build + 'css',
            watch: base + 'style/**/*.scss'
        },
        js: {
            jsInternal:  base + 'js/internal.js',
            jsExternal: base + 'js/external.js',
            
            dest: build + 'js',
            watch: base + 'js/**/*.js',
            eslint: base + '/js/**/*.js',
        },
        images: {
            src:  base + 'images/*.{jpg,jpeg,png,gif}',
            dest: build + 'images'
        },
        svg: {
            src:  base + 'images/svg/*.svg',
            dest:  build + 'images/svg/',
            
            sprite: base + 'images/svgSprite/*.svg',
            spriteAnimation: base + 'images/svgSpriteAnimation/*.svg',
            destSprite:  base + 'images/svg/'
        },
        fonts: {
            src:  base + 'fonts/**/*{.woff,.eot,.woff2,.ttf}',
            dest: build + 'fonts'
        }
    };

    
module.exports = {
    build: build,
    base: base,
    html: {        
        src: path.html.src,
        dest: path.html.dest,
    },
    css: {
        styleInternal: path.css.styleInternal,
        styleExternal: path.css.styleExternal,
        
        dest: path.css.dest,
        watch: path.css.watch
    },
    js: {
        jsInternal:  path.js.jsInternal,
        jsExternal: path.js.jsExternal,
        
        dest: path.js.dest,
        watch: path.js.watch,
        eslint: path.js.eslint
    },
    images: {
        src:  path.images.src,
        dest: path.images.dest
    },
    svg: {
        src:  path.svg.src,
        dest:  path.svg.dest,

        sprite: path.svg.sprite,
        spriteAnimation: path.svg.spriteAnimation,
        destSprite: path.svg.destSprite
    },
    fonts: {
        src:  path.fonts.src,
        dest: path.fonts.dest
    }
}