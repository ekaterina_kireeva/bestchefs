var gulp  = require('gulp'),
    browserSync = require('browser-sync').create(),

    config = require('../config');

gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: config.build
        },
        port: 8080,
        open: false
    });
});

gulp.task('dev', ['build', 'browser-sync'], function () {
    gulp.watch(config.images.src, ["images"]);
    gulp.watch(config.images.src, ["svg"]);
    gulp.watch(config.css.watch, ['style']);
    gulp.watch(config.js.watch, ['js']);
    gulp.watch(config.html.src, ['jade']);
});

gulp.task('build', ['fonts', 'images', 'svg', 'style', 'js', 'jade']);