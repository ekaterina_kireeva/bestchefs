var gulp  = require('gulp'),
    browserSync = require('browser-sync').create(),
    eslint = require('gulp-eslint'),
    styleLint = require('gulp-stylelint');

    config = require('../config');

/*
 * Eslint
 */
gulp.task('lint', ['eslint', 'style-lint']);

gulp.task('style-lint', function sassLintTask() {
    return gulp.src(config.css.watch)
        .pipe(styleLint({
            configFile    : '.stylelintrc',
            failAfterError: false,
            debug         : true,
            syntax        : 'scss',
            reporters     : [
                {formatter: 'string', console: true}
            ]
        }));
});

gulp.task('eslint', function() {
    return gulp.src(config.js.eslint)
        .pipe(eslint({
            configFile: '.eslintrc'
        }))
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});