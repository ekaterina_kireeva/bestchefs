var gulp  = require('gulp'),
    browserSync = require('browser-sync').create(),
    fileinclude = require('gulp-file-include'), //импорт файлов в файл
    notify = require('gulp-notify'),
    plumber = require('gulp-plumber'),
    babel = require('gulp-babel'),

    config = require('../config');
    
/*
 * Сборка js
 */
gulp.task('js', [
    'js-internal',
    'js-external'
]);

/*
 * Сборка js файлов
 */
gulp.task('js-internal', function() {
    browserSync.notify('Compiling js internal');

    return gulp.src(config.js.jsInternal)
        .pipe(fileinclude({
            prefix: '@@',
            baseconfig: '@file',
            indent: true
        }))
        .pipe(plumber({
            errorHandler: notify.onError({
                message: 'Line: <%= error.lineNumber %>:' +
                ' <%= error.message %>' +
                '\n<%= error.fileName %>',
                title: '<%= error.plugin %>'
            })
        }))
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(plumber.stop())
        .pipe(gulp.dest(config.js.dest))
        .pipe(browserSync.stream());
});

/*
 * Сборка js файлов - библиотек
 */
gulp.task('js-external', function() {
    browserSync.notify('Compiling js external');    
    
    return gulp.src(config.js.jsExternal)
        .pipe(fileinclude({
            prefix: '@@',
            baseconfig: '@file',
            indent: true
        }))
        .pipe(gulp.dest(config.js.dest))
        .pipe(browserSync.stream());
});