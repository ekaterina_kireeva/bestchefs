var gulp  = require('gulp'),
	browserSync = require('browser-sync').create(),
	
    config = require('../config');

/*
 * Перенос шрифтов из папки /src/fonts/
 */
gulp.task('fonts', function(){
	gulp.src(config.fonts.src)
		.pipe(gulp.dest(config.fonts.dest));
});