var gulp  = require('gulp'),
    browserSync = require('browser-sync').create(),
    jade = require('gulp-pug'),
    typograf = require('gulp-typograf'),
    
    config = require('../config');
    
/*
 * Сборка html из папки /src/*.jade
 */
gulp.task('jade', ['svg'], function () { //ждем пока создастся спрайт, чтобы включить в страницу
    gulp.src(config.html.src)
        .pipe(jade({
            basedir: config.base,
            pretty: true
        }))
        .pipe(typograf({
            locale: ['ru', 'en-US'],
            disable: ['ru/nbsp/centuries', 'common/number/fraction', 'ru/other/phone-number']
        }))
        .pipe(gulp.dest(config.html.dest));
});
