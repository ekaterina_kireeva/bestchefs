var gulp  = require('gulp'),
    browserSync = require('browser-sync').create(),
    svgSprite = require('gulp-svg-sprites'), //создание спрайта
	svgmin = require('gulp-svgmin'), //минификация SVG
    cheerio = require('gulp-cheerio'),//удаление лишних атрибутов из svg
	replace = require('gulp-replace'), 

    config = require('../config');


/*
 * Сборка стилей
 */
gulp.task('svg', [
    'generte-sprite-svg',
    'svgstore'
]);

/*
 * Сборка svg-изображений из файлов /src/images/svg/*.svg
 */
gulp.task('svgstore', ['generte-sprite-svg'], function () { 
    browserSync.notify('Compiling svg');

    gulp.src(config.svg.src) 
        .pipe(gulp.dest(config.svg.dest));
});

/*
 * Генерация svg спрайт из файлов /src/images/svgSprite/*.svg
 */
gulp.task('generte-sprite-svg', function () {
    return gulp.src(config.svg.sprite)
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        .pipe(svgSprite({
            mode: 'symbols',
            preview: false,
            selector: 'icon-%f',
            cssFile: false,
            svg: {
                sprite: 'sprite.svg',
                symbols: 'sprite.svg'
            },
            preview: false
        }))
        .pipe(gulp.dest(config.svg.destSprite));
});

/*
 * Генерация svg спрайт из файлов /src/images/svgSpriteAnimation/*.svg
 */
gulp.task('generte-animation-sprite-svg', function () {
    return gulp.src(config.svg.spriteAnimation)
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        .pipe(svgSprite({
            selector: 'icon-%f',
            svg: {
                sprite: 'sprite-animation.svg'
            },
            preview: false
        }))
        .pipe(gulp.dest(config.svg.destSprite));
});