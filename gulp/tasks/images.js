var gulp  = require('gulp'),
    browserSync = require('browser-sync').create(),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),

    config = require('../config');

/*
 * Сборка изображений из файлов /src/images/*.{jpg,jpeg,png,gif}
 */
gulp.task('images', [
    'image-min',
    'image-copy'
]);

gulp.task('image-min', function () {
    browserSync.notify('Compiling images');

    gulp.src(config.images.src)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{
                removeViewBox: false
            }],
            use: [pngquant()]
        }));
});

gulp.task('image-copy', ['image-min'], function () {
    browserSync.notify('Compiling images');

    gulp.src(config.images.src)
        .pipe(gulp.dest(config.images.dest));
});