var gulp  = require('gulp'),
    browserSync = require('browser-sync').create(),
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    flexbugsfixes = require('postcss-flexbugs-fixes'),
    autoprefixer = require('autoprefixer'),
    notify = require('gulp-notify'),
    cleanCSS = require('gulp-clean-css'),
    combineMq = require('gulp-combine-mq'),

    config = require('../config');

/*
 * Сборка стилей
 */
gulp.task('style', [
    'sass-internal',
    'sass-external'
]);

/*
 * Сборка самописных стилей
 */
gulp.task('sass-internal', function () {
    browserSync.notify('Compiling sass internal');
    
    gulp.src(config.css.styleInternal)
        .pipe(sass())
        .on('error', notify.onError({
            message: 'Line: <%= error.lineNumber %>:' +
            ' <%= error.message %>' +
            '\n<%= error.fileName %>',
            title: '<%= error.plugin %>'
        }))
        .on('error', function() {
            this.emit('end');
        })
        .pipe(postcss([
            flexbugsfixes(),
            autoprefixer({
                browsers: ['last 3 versions'],
                cascade : false
            })
        ]))
        .pipe(combineMq())          
        .pipe(cleanCSS({
            format: 'beautify'
        }))
        .pipe(gulp.dest(config.css.dest))
        .pipe(browserSync.stream());
});

/*
 * Сборка стилей библиотек
 */
gulp.task('sass-external', function() {
    browserSync.notify('Compiling sass external');    
    
    return gulp.src(config.css.styleExternal)
        .pipe(sass())
        .on('error', notify.onError({
            message: 'Line: <%= error.lineNumber %>:' +
            ' <%= error.message %>' +
            '\n<%= error.fileName %>',
            title: '<%= error.plugin %>'
        }))
        .on('error', function() {
            this.emit('end');
        })
        .pipe(gulp.dest(config.css.dest))
        .pipe(browserSync.stream());
});